package com.example.afinal

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
// ...
// Initialize Firebase Auth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        auth = FirebaseAuth.getInstance()


        init()
    }


    private fun init(){
        RegBtn.setOnClickListener {

            val intent = Intent(this, Main2Activity::class.java)
            startActivity(intent)
            finish()
        }

        LoginBtn.setOnClickListener {
            signin()


        }
    }
    private fun signin() {
        if (EmailEditText.text.toString().isEmpty()) {
            EmailEditText.error = "Please enter email"
            EmailEditText.requestFocus()
            return
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(EmailEditText.text.toString()).matches()) {
            EmailEditText.error = "Please enter True email"
            EmailEditText.requestFocus()
            return
        }
        if (PasswordeditText.text.toString().isEmpty()) {
            PasswordeditText.error = "Please enter password"
            PasswordeditText.requestFocus()
            return
        }
        auth.signInWithEmailAndPassword(EmailEditText.text.toString(), PasswordeditText.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    updateUI(null)
                }

            }

    }

    public override fun onStart() {
        super.onStart()
        val currentUser: FirebaseUser? = auth.currentUser
        updateUI(currentUser)
    }

    private fun updateUI(currentUser: FirebaseUser?) {
        if (currentUser != null) {
            if (currentUser.isEmailVerified) {
                startActivity(Intent(this, Main3Activity::class.java))
                finish()

            } else {
                Toast.makeText(
                    baseContext, "please verify your email address.",
                    Toast.LENGTH_SHORT
                ).show()

            }
        } else {
            Toast.makeText(
                baseContext, "hello on Obiectivi news",
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}
