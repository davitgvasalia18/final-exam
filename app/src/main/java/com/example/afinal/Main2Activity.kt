package com.example.afinal

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_main2.*

class Main2Activity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        init()
        auth = FirebaseAuth.getInstance()
    }

    private fun init(){
        backbtn.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

        RegistrationBtn.setOnClickListener {
            signUpUser()

        }

    }
    private fun signUpUser() {
        if (Emailedittext.text.toString().isEmpty()) {
            Emailedittext.error = "Please enter email"
            Emailedittext.requestFocus()
            return
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(Emailedittext.text.toString()).matches()) {
            Emailedittext.error = "Please enter valid email"
            Emailedittext.requestFocus()
            return
        }
        if (Passwordedittext.text.toString().isEmpty()) {
            Passwordedittext.error = "Please enter password"
            Passwordedittext.requestFocus()
            return
        }



        auth.createUserWithEmailAndPassword(
            Emailedittext.text.toString(),
            Passwordedittext.text.toString()
        )
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user: FirebaseUser? = auth.currentUser
                    user?.sendEmailVerification()
                        ?.addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                startActivity(Intent(this, MainActivity::class.java))

                                finish()

                            }
                        }

                } else {
                    Toast.makeText(
                        baseContext, "Sign Up failed. Try again after some time.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
    }

}
